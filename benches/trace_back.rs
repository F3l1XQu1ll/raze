use criterion::{criterion_group, Criterion};
use razes_lib::trace_back_iterative;

use razes_lib::{bfs, load_maze};

pub fn bench_trace_back(c: &mut Criterion) {
    let maze = load_maze("mazes/maze2.mz").expect("Should be able to load maze 2");
    let start = maze.entrace();
    let ends = maze.exits();
    let bfs = bfs(&maze, &start, &ends);

    c.bench_function("trace_back_iterative", |b| {
        b.iter(|| trace_back_iterative(&maze, &bfs))
    });
}

criterion_group!(tb_benches, bench_trace_back);
