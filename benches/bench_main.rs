use criterion::criterion_main;

mod bfs;
mod trace_back;

criterion_main! {bfs::bfs_benches, trace_back::tb_benches}
