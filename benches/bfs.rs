use criterion::{criterion_group, Criterion};

use razes_lib::{bfs, load_maze};

pub fn bench_bfs(c: &mut Criterion) {
    let maze = load_maze("mazes/maze2.mz").expect("Should be able to load maze 2");
    let start = maze.entrace();
    let ends = maze.exits();
    c.bench_function("bfs", |b| b.iter(|| bfs(&maze, &start, &ends)));
}

criterion_group!(bfs_benches, bench_bfs);
