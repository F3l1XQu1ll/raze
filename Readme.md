# Razes

Razes is a (mandatory :rocket:) (also mandatory) ***blazingly fast*** maze-solving application written in Rust.

# Implementation

The solving algorithm in `razes_lib` is based on a BFS and backtrace algorithm.

# Performance

Benchmarks are implemented (don't trust the runtime outputs, run `cargo bench`), see yourself.