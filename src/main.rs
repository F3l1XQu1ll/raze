use clap::Parser;
use razes_lib::bfs;
use razes_lib::load_maze;
use razes_lib::steps_to_moves;
use razes_lib::trace_back_iterative;
use razes_lib::CellType;
use razes_lib::Direction;
use std::time::Instant;

macro_rules! benchmark {
    ($code:expr, $msg:expr) => {{
        let now = Instant::now();
        let result = $code;
        let elapsed = now.elapsed();
        println!("{}, took {elapsed:#?}", $msg);
        result
    }};
}

/// Argument parsing
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path to maze file
    maze_path: String,

    /// Display the loaded maze
    #[arg(short, long, default_value_t = false)]
    display_maze: bool,

    /// Display maze after BFS invocation
    #[arg(short, long, default_value_t = false)]
    bfs_display_maze: bool,

    /// Display solved maze
    #[arg(long)]
    no_display_solved_maze: bool,

    /// Display moves
    #[arg(short, long, default_value_t = false)]
    moves: bool,
}

fn main() {
    let args = Args::parse();

    let maze =
        benchmark! {load_maze(args.maze_path).expect("Maze could not be loaded"), "Loaded maze"};
    if args.display_maze {
        maze.dump_maze();
    }

    let bfs = benchmark! { bfs(&maze, maze.entrace(), &maze.exits()), "BFS done"};
    println!(
        "\nFound end! Min Dist = {}",
        bfs.last()
            .expect("BFS result should contain at least one element")
            .1
    );

    if args.bfs_display_maze {
        let mut bfs_maze = maze.clone();
        for (cell, distance) in &bfs {
            bfs_maze.cells[*cell].c_type = CellType::Solved;
            bfs_maze.cells[*cell].dbg_highlight = Some(*distance);
        }
        bfs_maze.dump_maze();
    }

    let steps = benchmark! { trace_back_iterative(&maze, &bfs), "Path construction done"};

    if !args.no_display_solved_maze {
        let mut solved_maze = maze.clone();
        for (cell, distance) in &steps {
            solved_maze.cells[*cell].c_type = CellType::Solved;
            solved_maze.cells[*cell].dbg_highlight = Some(*distance);
        }
        solved_maze.dump_maze();
    }

    if args.moves {
        for direction in benchmark! {steps_to_moves(&maze, &steps),"Steps to moves conversion done"}
        {
            let direction_string = match direction {
                Direction::North => "North",
                Direction::South => "South",
                Direction::East => "East",
                Direction::West => "West",
            };
            println!("{direction_string}");
        }
    }
}
