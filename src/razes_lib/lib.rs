use std::{collections::VecDeque, fs::File, io::Read, path::Path};

use anyhow::Result;

/// Describes the type of a cell in a maze.
/// Cells can be emtpy, blocking or special.
/// Special cells are the entrace and exit cell.
/// We also introduce a "solved" type for visualisation purposes.
#[derive(PartialEq, Eq, Debug, Clone)]
pub enum CellType {
    Empty,    // ' '
    Blocking, // '#', '-', '|'
    Entrace,  // 'S'
    Exit,     // 'X'
    Solved,   // '*'
}

/// Describes direction for movement in solved mazes
pub enum Direction {
    North,
    South,
    East,
    West,
}

/// A Cell in a maze.
/// Cells are categorized by their type.
/// Cells can have neighboring cells.
/// Cells can be highlighted by a factor.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct MazeCell {
    /// Type of the Cell
    pub c_type: CellType,
    /// Highlighting factor of the cell
    /// None means no highlighting.
    pub dbg_highlight: Option<u32>,
    /// Index of the cell
    /// This must be the index of the cell in the maze!
    pub index: usize,
    /// Possible neighbor in the north
    pub north: Option<usize>,
    /// Possible neighbor in the south
    pub south: Option<usize>,
    /// Possible neighbor in the east
    pub east: Option<usize>,
    /// Possible neighbor in the west
    pub west: Option<usize>,
}

impl MazeCell {
    /// A cell is considered "empty" / a valid path if it is truly empty or the exit point.
    pub fn is_empty(&self) -> bool {
        self.c_type == CellType::Empty || self.c_type == CellType::Exit
    }
}

/// A maze is a collection of cells
/// Each cell is relative to 2 to 4 other cells
///
/// # Implementation:
///
/// Cells are stored as one continous array.
/// Each cell from the top left to the bottom right of the maze is numbered starting at zero from left to right and top to bottom.
/// Each cell also has neighbors in the north, south, east and west (if they exist).
#[derive(Clone)]
pub struct Maze {
    pub cells: Vec<MazeCell>,
}
impl Maze {
    /// Finds the entrace
    ///
    /// If there are multible entraces in the maze this will return the first found.
    /// Such mazes are not part of the spec.
    ///
    /// # Panics
    ///
    /// Panics if there is no entrace cell in the maze
    pub fn entrace(&self) -> &MazeCell {
        let entrace = self
            .cells
            .iter()
            .find(|cell| cell.c_type == CellType::Entrace)
            .expect("There should be an entrace.");
        entrace
    }

    /// Finds the exits
    ///
    /// The list will be empty if no exits where found
    pub fn exits(&self) -> Vec<&MazeCell> {
        self.cells
            .iter()
            .filter(|c| c.c_type == CellType::Exit)
            .collect()
    }

    /// Loads the cell from the maze if a valid cell id was given and the cell exists in the maze
    pub fn get_empty_cell(&self, cell_id: Option<usize>) -> Option<&MazeCell> {
        cell_id
            .and_then(|c| self.cells.get(c))
            .filter(|c| c.is_empty())
    }

    /// Writes the maze to stdout, applying highlighting if given
    pub fn dump_maze(&self) {
        for cell in &self.cells {
            if let Some(hightlight) = cell.dbg_highlight {
                print!(
                    "\x1b[38;2;255;{h};{h}m",
                    h = hightlight // 255.0 - (255.0 * (hightlight as f32 / 54.0)).floor()
                );
            }
            print!(
                "{}",
                match cell.c_type {
                    CellType::Empty => " ",
                    CellType::Blocking => "#",
                    CellType::Entrace => "S",
                    CellType::Exit => "X",
                    CellType::Solved => "*",
                },
            );
            if cell.dbg_highlight.is_some() {
                print!("\x1b[0m");
            }
            if cell.east.is_none() {
                println!();
            }
        }
    }
}

/// Try to load and parse a maze file into a maze
pub fn load_maze(path: impl AsRef<Path>) -> Result<Maze> {
    let mut maze_file = File::open(path.as_ref())?;

    let mut maze_data = String::new();
    maze_file.read_to_string(&mut maze_data)?;

    // tim_end to get rid of the last linebreak
    let maze_lines = maze_data.trim_end().split('\n').collect::<Vec<_>>();

    let line_length = maze_lines
        .first()
        .expect("There should be at least one line in the maze.")
        .len();

    // Let's try to spare some allocations
    let mut maze_cells = Vec::with_capacity(maze_lines.len() * line_length);

    for (line_index, line) in maze_lines.into_iter().enumerate() {
        // Make sure all lines have equal length
        assert_eq!(line_length, line.len(), "The line {line_index} should have the same length as the first (0) line: {line_length}!");

        for (cell_index, cell) in line.char_indices() {
            let cell_type = match cell {
                ' ' => CellType::Empty,
                '#' | '-' | '|' => CellType::Blocking,
                'S' => CellType::Entrace,
                'X' => CellType::Exit,
                _ => panic!(
                    "Invalid cell type at index: {}",
                    line_index * line_length + cell_index
                ),
            }; // parse type of cell

            let current_index = line_index * line_length + cell_index;
            let north_index = current_index.checked_sub(line_length);
            let west_index = if current_index % line_length != 0 {
                current_index.checked_sub(1)
            } else {
                None
            };

            // append new cell to collection.
            maze_cells.push(MazeCell {
                c_type: cell_type,
                dbg_highlight: None,
                index: current_index,
                north: north_index,
                south: None, // We can not know this yet
                east: None,  // We can not know this yet
                west: west_index,
            });

            // Set south cell of north cell
            north_index.map(|north_index| {
                maze_cells
                    .get_mut(north_index)
                    .map(|c| c.south = Some(current_index))
            });
            // Set east cell of west cell
            west_index.map(|west_index| {
                maze_cells
                    .get_mut(west_index)
                    .map(|c| c.east = Some(current_index))
            });
        }
    }

    // Make sure only one entrace exists
    assert_eq!(
        maze_cells
            .iter()
            .filter(|cell| cell.c_type == CellType::Entrace)
            .count(),
        1,
        "There shold be one entrace in the maze!"
    );
    // Make sure at least one exit exists
    assert!(
        maze_cells.iter().any(|cell| cell.c_type == CellType::Exit),
        "There should be at least one exit in the maze!"
    );
    Ok(Maze { cells: maze_cells })
}

/// BFS algorithm to find the minimum length for a path through the maze
///
/// # Returns
///
/// Returns a list of tuples of cell index and corresponding distance from the start cell (entrace)
pub fn bfs<'a>(maze: &'a Maze, start: &'a MazeCell, ends: &[&'a MazeCell]) -> Vec<(usize, u32)> {
    // queue for bfs algorithm
    let mut queue = VecDeque::with_capacity(maze.cells.len());
    queue.push_back((start.clone(), 0u32));
    // let mut queue = VecDeque::from([(start, 0u32)]);

    // list of cells (cell ids) visited by the algorithm
    let mut visited = vec![false; maze.cells.len()];
    *visited
        .get_mut(start.index)
        .expect("The index of the starting cell should be in the array") = true;
    // let mut visisted = vec![start.index];

    // resulting list of cells and distances
    // We waste a bit of memory here, but overall we spare allocations
    let mut bfs = Vec::with_capacity(maze.cells.len());

    // indices of end cells for faster lookup
    let end_ids = ends.iter().map(|end| end.index).collect::<Vec<_>>();

    while !queue.is_empty() {
        // should always be Some as we only run this if the queue is not empty
        if let Some((cell, distance)) = queue.pop_front() {
            bfs.push((cell.index, distance));

            // check if the current cell is an exit
            if end_ids.contains(&cell.index) {
                // if we have hit an exit, we are done regardless of other exits with higher distance
                break;
            }
            // closure to avoid code duplication and a huge function header
            let mut enqueue_if_empty = |next_cell_id: Option<usize>| {
                next_cell_id
                    .and_then(|next_cell_id| maze.get_empty_cell(Some(next_cell_id)).cloned())
                    .filter(|next_cell| Some(&false) == visited.get(next_cell.index))
                    .map(|next_cell| {
                        *visited.get_mut(next_cell.index).unwrap_or_else(|| {
                            panic!("The cell {} should be visitable", next_cell.index)
                        }) = true;
                        queue.push_back((next_cell, distance + 1));
                    })
            };

            // enqueue cell in the north, south, east, west and mark as visited if the cell is empty
            enqueue_if_empty(cell.north);
            enqueue_if_empty(cell.south);
            enqueue_if_empty(cell.east);
            enqueue_if_empty(cell.west);
        }
    }

    bfs
}

/// Construct a path from the data supplied by the bfs algorithm
pub fn trace_back_iterative(maze: &Maze, bfs: &Vec<(usize, u32)>) -> Vec<(usize, u32)> {
    // get the id of the exit node and  maximum distance from the bfs results
    let (exit_node_id, max_distance) = bfs
        .last()
        .expect("BFS should have returned at least one element");
    // Minimum steps required to solve the maze equal the maximum distance from bfs
    let mut steps = Vec::with_capacity(*max_distance as usize);
    // last distance is the distance from the last step to the starting point
    // we initialize it to the maximum distance as this is the distance of the exit point
    let mut last_distance = *max_distance;
    // the exit node
    let exit_node = maze
        .cells
        .get(*exit_node_id)
        .expect("Exit node should be in the maze");
    // store the valid neighbors for the next iteration
    let mut next_nsew = [
        exit_node.north,
        exit_node.south,
        exit_node.east,
        exit_node.west,
    ];
    // store exit node as step
    steps.push((*exit_node_id, *max_distance));
    for cursor in (0..(bfs.len() - 1)).rev() {
        // get the id and corresponding distance from the bfs results at the current cursor
        let (current_node_id, current_distance) = bfs.get(cursor).expect("Node should be in bfs");
        // check if the current distance is equal to the last measured distance. (Or greater but this should never happen)
        // If the distances are equal, this means, we have not got closer to the starting point and we can skip this cell
        if *current_distance >= last_distance {
            continue;
        } else {
            // load node information from the maze
            let current_node = maze
                .cells
                .get(*current_node_id)
                .expect("Current node should be in the maze");
            // easy access to variables
            let [next_north, next_south, next_east, next_west] = next_nsew;

            // define the iterator closure here for better readability
            let next_node_is_neighbor =
                |next_node_neighbor: Option<usize>, current_node_id: usize| {
                    // if this neighbor does not exist, we can skip the check
                    if let Some(next_node_neighbor) = next_node_neighbor {
                        // return if this cell is the searched neighbor
                        next_node_neighbor == current_node_id
                    } else {
                        false
                    }
                };
            // iterate over possible neighbors and check if they are the current node
            if [next_north, next_south, next_east, next_west]
                .iter()
                .any(|next| next_node_is_neighbor(*next, *current_node_id))
            {
                // set the next neighbors
                next_nsew = [
                    current_node.north,
                    current_node.south,
                    current_node.east,
                    current_node.west,
                ];
                // set new distance (smaller by one than the last one)
                last_distance = *current_distance;
                // remember step
                steps.push((*current_node_id, *current_distance));
            } else {
                continue;
            }
        }
    }

    steps
}

/// Turn the (reverse) list of steps to solve the maze into movement instructions
pub fn steps_to_moves(maze: &Maze, steps: &Vec<(usize, u32)>) -> Vec<Direction> {
    let mut directions = Vec::with_capacity(steps.len());
    let mut steps_peekable = steps.iter().rev().peekable();
    while let Some((cell_id, _)) = steps_peekable.next() {
        let cell = maze
            .cells
            .get(*cell_id)
            .expect("Cell {cell_id} shold be in the maze.");
        match steps_peekable.peek() {
            Some((next_id, _)) => {
                if let Some(true) = cell.north.map(|n| n == *next_id) {
                    directions.push(Direction::North);
                } else if let Some(true) = cell.south.map(|s| s == *next_id) {
                    directions.push(Direction::South);
                } else if let Some(true) = cell.east.map(|e| e == *next_id) {
                    directions.push(Direction::East);
                } else if let Some(true) = cell.west.map(|w| w == *next_id) {
                    directions.push(Direction::West);
                } else {
                    panic!("Cell {next_id} should be a neighbor of cell {cell_id}!");
                }
            }
            None => break,
        }
    }
    directions
}
